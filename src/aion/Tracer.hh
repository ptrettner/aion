#pragma once

#include <cstdint>
#include <string>
#include <thread>

#ifdef _MSC_VER
#include <intrin.h>
#endif

/**
 * Main Macro: TRACE(...)
 *
 * See https://godbolt.org/z/qs33MN
 *
 * Usage:
 *   int foo() {
 *      TRACE();
 *
 *      return do_stuff();
 *   }
 *
 * Overhead: ~70-105 cycles
 *
 * TODO: low-overhead version without alloc_chunk check (~80 cycles)
 */
#define TRACE(...)                                                                                                                        \
    (void)__VA_ARGS__ " has to be a string literal";                                                                                      \
    static aion::tracing::location TRACER_MACRO_JOIN(_action_label, __LINE__) = {__FILE__, TRACER_PRETTY_FUNC, "" __VA_ARGS__, __LINE__}; \
    aion::tracing::detail::raii_profiler TRACER_MACRO_JOIN(_action_, __LINE__)(&TRACER_MACRO_JOIN(_action_label, __LINE__))

// Implementation:

#define TRACER_TRACE_SIZE 9
#define TRACER_LABEL_MASK 0x80000000
#define TRACER_END_VALUE 0xFFFFFFFF

#define TRACER_MACRO_JOIN_IMPL(arg1, arg2) arg1##arg2
#define TRACER_MACRO_JOIN(arg1, arg2) TRACER_MACRO_JOIN_IMPL(arg1, arg2)

#ifndef _MSC_VER

#define TRACER_FORCEINLINE __attribute__((always_inline))
#define TRACER_NOINLINE __attribute__((noinline))
#define TRACER_PRETTY_FUNC __PRETTY_FUNCTION__

#define TRACER_LIKELY(x) __builtin_expect((x), 1)
#define TRACER_UNLIKELY(x) __builtin_expect((x), 0)
#define TRACER_COLD __attribute__((cold))

#else

#define TRACER_FORCEINLINE __forceinline
#define TRACER_NOINLINE __declspec(noinline)
#define TRACER_PRETTY_FUNC __FUNCTION__

#define TRACER_LIKELY(x) x
#define TRACER_UNLIKELY(x) x
#define TRACER_COLD

#endif

namespace aion
{
namespace tracing
{
/// sets the size of newly allocated chunks
/// is a per-thread setting
void set_thread_chunk_size(size_t size, float growth_factor = 1.6f, size_t max_size = 10 * (1 << 20));
/// user-defined name for this thread
void set_thread_name(std::string const& name);

struct location
{
    char const* file;
    char const* function;
    char const* name;
    int line;
};

/// visitor base class, call order is:
/// on_thread
///   -> nested on_trace_start .. on_trace_end
/// traces might not have _end if they are still running
struct visitor
{
    virtual void on_thread(std::thread::id thread) {}
    virtual void on_trace_start(location* loc, uint64_t cycles, uint32_t cpu) {}
    virtual void on_trace_end(uint64_t cycles, uint32_t cpu) {}
};

void visit_thread(visitor& v);

#ifdef _WIN32
inline uint64_t current_cycles() { return __rdtsc(); }
#else //  Linux/GCC
inline uint64_t current_cycles()
{
    unsigned int lo, hi;
    __asm__ __volatile__("rdtsc" : "=a"(lo), "=d"(hi));
    return ((uint64_t)hi << 32) | lo;
}
#endif

/// writes all trace points to a json file
/// Format:
/// {
///     "locations": [
///         {
///             "file": "...",
///             "function": "...",
///             "name": "...",
///             "line": 42
///         },
///         ...
///     ],
///     "threads": [
///         {
///             "name": "...",
///             "id": 123456,
///             "trace": [
///                 {
///                     "loc": 0,   <-- index into location array
///                     "start": 123456789,
///                     "end": 123456789,
///                     "cpu_start": 7,
///                     "cpu_end": 7,
///                     "trace": [
///                         ...     <-- nested trace array
///                     ]
///                 },
///                 ...
///             ]
///         },
///         ...
///     ]
/// }
void write_json(std::string const& filename);
/// writes a csv where all trace points are summarized per-location
void write_summary_csv(std::string const& filename);
/// generates a Brendan Gregg's collapsed stack format file
/// see https://github.com/BrendanGregg/flamegraph#2-fold-stacks
/// e.g. for use with https://github.com/jlfwong/speedscope
/// recommended file name is .folded
void write_collapsed_stack(std::string const& filename);
/// Json file for use with https://github.com/jlfwong/speedscope
/// see https://github.com/jlfwong/speedscope/wiki/Importing-from-custom-sources
void write_speedscope_json(std::string const& filename = "speedscope.json");
/// writes various output formats to a given directory
/// NOTE: does nothing if directory does not exist
void write_dir(std::string const& path);

namespace detail
{
struct thread_data
{
    uint32_t* curr;
    uint32_t* end; ///< not actually end, has a TRACER_TRACE_SIZE buffer at the end
};

TRACER_COLD TRACER_NOINLINE uint32_t* alloc_chunk();

inline thread_data& tdata()
{
    static thread_local thread_data data = {nullptr, nullptr};
    return data;
}

struct raii_profiler
{
    raii_profiler(location* loc)
    {
        auto pd = tdata().curr;
        if (TRACER_UNLIKELY(pd >= tdata().end)) // alloc new chunk
            pd = alloc_chunk();
        tdata().curr = pd + 5;

        *(location**)pd = loc;

        unsigned int core;
#ifdef _MSC_VER
        int64_t cc = __rdtscp(&core);
        *(int64_t*)(pd + 2) = cc;
#else
        unsigned int lo, hi;
        __asm__ __volatile__("rdtscp" : "=a"(lo), "=d"(hi), "=c"(core));
        pd[2] = lo;
        pd[3] = hi;
#endif
        pd[4] = core;
    }

    ~raii_profiler()
    {
        auto pd = tdata().curr;
        if (TRACER_UNLIKELY(pd >= tdata().end)) // alloc new chunk
            pd = alloc_chunk();
        tdata().curr = pd + 4;

        unsigned int core;
#ifdef _MSC_VER
        int64_t cc = __rdtscp(&core);
        pd[0] = TRACER_END_VALUE;
        *(int64_t*)(pd + 1) = cc;
#else
        unsigned int lo, hi;
        __asm__ __volatile__("rdtscp" : "=a"(lo), "=d"(hi), "=c"(core));
        pd[0] = TRACER_END_VALUE;
        pd[1] = lo;
        pd[2] = hi;
#endif
        pd[3] = core;
    }
};
} // namespace detail
} // namespace tracing
} // namespace aion
